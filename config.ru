require 'bundler/setup'

$LOAD_PATH.unshift 'lib'

require 'app'

map '/' do
  run App
end

if App.development?
  map '/gnome-assets' do
    run App.sprockets
  end
end
